from django.shortcuts import render, redirect
from django.views.generic import View
from .form import *
from django.contrib import messages
from django.contrib.auth import authenticate, login


# Create your views here.

def home(request):
    return render(request, 'home.html')


class SignupView(View):
    def get(self, request):
        form = SignUpForm()
        return render(request, 'signup.html', {'form': form})

    def post(self, request):
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, "Sign Up Success !")
            return redirect('/signup')
        else:
            return render(request, 'signup.html', {'form': form})


class LoginView(View):
    def get(self, request):
        form = MyLoginForm()
        return render(request, 'login.html', {'form': form})

    def post(self, request):
        form = MyLoginForm(request, data=request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']

            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect('/')
            else:
                return render(request, 'login.html', {'form': form})
        else:
            return render(request, 'login.html', {'form': form})
